package com.gomefinance.consumerfinance.pmc.dal.model;

import java.util.Date;

public class NRoutWeightConfig {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 支付公司编号
     */
    private String payCompanyCode;

    /**
     * 支付产品编号
     */
    private String payProductCode;

    /**
     * 银行编号
     */
    private String bankCode;

    /**
     * 支付类型编号
     */
    private String payType;

    /**
     * 权重
     */
    private Integer weight;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 主键
     * @return id 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 支付公司编号
     * @return pay_company_code 支付公司编号
     */
    public String getPayCompanyCode() {
        return payCompanyCode;
    }

    /**
     * 支付公司编号
     * @param payCompanyCode 支付公司编号
     */
    public void setPayCompanyCode(String payCompanyCode) {
        this.payCompanyCode = payCompanyCode == null ? null : payCompanyCode.trim();
    }

    /**
     * 支付产品编号
     * @return pay_product_code 支付产品编号
     */
    public String getPayProductCode() {
        return payProductCode;
    }

    /**
     * 支付产品编号
     * @param payProductCode 支付产品编号
     */
    public void setPayProductCode(String payProductCode) {
        this.payProductCode = payProductCode == null ? null : payProductCode.trim();
    }

    /**
     * 银行编号
     * @return bank_code 银行编号
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * 银行编号
     * @param bankCode 银行编号
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode == null ? null : bankCode.trim();
    }

    /**
     * 支付类型编号
     * @return pay_type 支付类型编号
     */
    public String getPayType() {
        return payType;
    }

    /**
     * 支付类型编号
     * @param payType 支付类型编号
     */
    public void setPayType(String payType) {
        this.payType = payType == null ? null : payType.trim();
    }

    /**
     * 权重
     * @return weight 权重
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * 权重
     * @param weight 权重
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    /**
     * 创建时间
     * @return create_time 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 操作人
     * @return operator 操作人
     */
    public String getOperator() {
        return operator;
    }

    /**
     * 操作人
     * @param operator 操作人
     */
    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", payCompanyCode=").append(payCompanyCode);
        sb.append(", payProductCode=").append(payProductCode);
        sb.append(", bankCode=").append(bankCode);
        sb.append(", payType=").append(payType);
        sb.append(", weight=").append(weight);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", operator=").append(operator);
        sb.append("]");
        return sb.toString();
    }
}