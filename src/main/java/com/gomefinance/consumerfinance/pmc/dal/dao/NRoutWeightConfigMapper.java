package com.gomefinance.consumerfinance.pmc.dal.dao;

import com.gomefinance.consumerfinance.pmc.dal.model.NRoutWeightConfig;

public interface NRoutWeightConfigMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(NRoutWeightConfig record);

    int insertSelective(NRoutWeightConfig record);

    NRoutWeightConfig selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(NRoutWeightConfig record);

    int updateByPrimaryKey(NRoutWeightConfig record);
}