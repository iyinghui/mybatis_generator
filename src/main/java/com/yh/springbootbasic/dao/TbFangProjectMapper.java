package com.yh.springbootbasic.dao;

import com.yh.springbootbasic.model.TbFangProject;

public interface TbFangProjectMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbFangProject record);

    int insertSelective(TbFangProject record);

    TbFangProject selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbFangProject record);

    int updateByPrimaryKey(TbFangProject record);
}