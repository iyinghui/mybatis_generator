package com.yh.springbootbasic.model;

import java.util.Date;

public class TbFangProject {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 最高单价
     */
    private Integer maxAmount;

    /**
     * 最低单价
     */
    private Integer minAmount;

    /**
     * 平均单价
     */
    private Integer averageAmount;

    /**
     * 地址
     */
    private String address;

    /**
     * 户型
     */
    private String floorType;

    /**
     * 类型：住宅/商户
     */
    private String type;

    /**
     * 地址-所在省份
     */
    private String addrProvince;

    /**
     * 地址-所在城市
     */
    private String addrCity;

    /**
     * 地址-所在区/县
     */
    private String addrDistrict;

    /**
     * 系统状态：正常/废弃
     */
    private String sysState;

    /**
     * 施工状态：未开始/进行中/停工/完成
     */
    private String buildState;

    /**
     * 商业状态：未开盘/在售/售罄...
     */
    private String busState;

    /**
     * 所在经度
     */
    private String longitude;

    /**
     * 所在纬度
     */
    private String latitude;

    /**
     * 销售电话
     */
    private String salesCall;

    /**
     * 动工日期
     */
    private Date buildStartDate;

    /**
     * 开盘日期
     */
    private Date openingDate;

    /**
     * 完工日期
     */
    private Date buildFinishDate;

    /**
     * 交房日期
     */
    private Date handInDate;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 主键
     * @return id 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 项目名称
     * @return project_name 项目名称
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * 项目名称
     * @param projectName 项目名称
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName == null ? null : projectName.trim();
    }

    /**
     * 最高单价
     * @return max_amount 最高单价
     */
    public Integer getMaxAmount() {
        return maxAmount;
    }

    /**
     * 最高单价
     * @param maxAmount 最高单价
     */
    public void setMaxAmount(Integer maxAmount) {
        this.maxAmount = maxAmount;
    }

    /**
     * 最低单价
     * @return min_amount 最低单价
     */
    public Integer getMinAmount() {
        return minAmount;
    }

    /**
     * 最低单价
     * @param minAmount 最低单价
     */
    public void setMinAmount(Integer minAmount) {
        this.minAmount = minAmount;
    }

    /**
     * 平均单价
     * @return average_amount 平均单价
     */
    public Integer getAverageAmount() {
        return averageAmount;
    }

    /**
     * 平均单价
     * @param averageAmount 平均单价
     */
    public void setAverageAmount(Integer averageAmount) {
        this.averageAmount = averageAmount;
    }

    /**
     * 地址
     * @return address 地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 地址
     * @param address 地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 户型
     * @return floor_type 户型
     */
    public String getFloorType() {
        return floorType;
    }

    /**
     * 户型
     * @param floorType 户型
     */
    public void setFloorType(String floorType) {
        this.floorType = floorType == null ? null : floorType.trim();
    }

    /**
     * 类型：住宅/商户
     * @return type 类型：住宅/商户
     */
    public String getType() {
        return type;
    }

    /**
     * 类型：住宅/商户
     * @param type 类型：住宅/商户
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 地址-所在省份
     * @return addr_province 地址-所在省份
     */
    public String getAddrProvince() {
        return addrProvince;
    }

    /**
     * 地址-所在省份
     * @param addrProvince 地址-所在省份
     */
    public void setAddrProvince(String addrProvince) {
        this.addrProvince = addrProvince == null ? null : addrProvince.trim();
    }

    /**
     * 地址-所在城市
     * @return addr_city 地址-所在城市
     */
    public String getAddrCity() {
        return addrCity;
    }

    /**
     * 地址-所在城市
     * @param addrCity 地址-所在城市
     */
    public void setAddrCity(String addrCity) {
        this.addrCity = addrCity == null ? null : addrCity.trim();
    }

    /**
     * 地址-所在区/县
     * @return addr_district 地址-所在区/县
     */
    public String getAddrDistrict() {
        return addrDistrict;
    }

    /**
     * 地址-所在区/县
     * @param addrDistrict 地址-所在区/县
     */
    public void setAddrDistrict(String addrDistrict) {
        this.addrDistrict = addrDistrict == null ? null : addrDistrict.trim();
    }

    /**
     * 系统状态：正常/废弃
     * @return sys_state 系统状态：正常/废弃
     */
    public String getSysState() {
        return sysState;
    }

    /**
     * 系统状态：正常/废弃
     * @param sysState 系统状态：正常/废弃
     */
    public void setSysState(String sysState) {
        this.sysState = sysState == null ? null : sysState.trim();
    }

    /**
     * 施工状态：未开始/进行中/停工/完成
     * @return build_state 施工状态：未开始/进行中/停工/完成
     */
    public String getBuildState() {
        return buildState;
    }

    /**
     * 施工状态：未开始/进行中/停工/完成
     * @param buildState 施工状态：未开始/进行中/停工/完成
     */
    public void setBuildState(String buildState) {
        this.buildState = buildState == null ? null : buildState.trim();
    }

    /**
     * 商业状态：未开盘/在售/售罄...
     * @return bus_state 商业状态：未开盘/在售/售罄...
     */
    public String getBusState() {
        return busState;
    }

    /**
     * 商业状态：未开盘/在售/售罄...
     * @param busState 商业状态：未开盘/在售/售罄...
     */
    public void setBusState(String busState) {
        this.busState = busState == null ? null : busState.trim();
    }

    /**
     * 所在经度
     * @return longitude 所在经度
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * 所在经度
     * @param longitude 所在经度
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude == null ? null : longitude.trim();
    }

    /**
     * 所在纬度
     * @return latitude 所在纬度
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * 所在纬度
     * @param latitude 所在纬度
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude == null ? null : latitude.trim();
    }

    /**
     * 销售电话
     * @return sales_call 销售电话
     */
    public String getSalesCall() {
        return salesCall;
    }

    /**
     * 销售电话
     * @param salesCall 销售电话
     */
    public void setSalesCall(String salesCall) {
        this.salesCall = salesCall == null ? null : salesCall.trim();
    }

    /**
     * 动工日期
     * @return build_start_date 动工日期
     */
    public Date getBuildStartDate() {
        return buildStartDate;
    }

    /**
     * 动工日期
     * @param buildStartDate 动工日期
     */
    public void setBuildStartDate(Date buildStartDate) {
        this.buildStartDate = buildStartDate;
    }

    /**
     * 开盘日期
     * @return opening_date 开盘日期
     */
    public Date getOpeningDate() {
        return openingDate;
    }

    /**
     * 开盘日期
     * @param openingDate 开盘日期
     */
    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    /**
     * 完工日期
     * @return build_finish_date 完工日期
     */
    public Date getBuildFinishDate() {
        return buildFinishDate;
    }

    /**
     * 完工日期
     * @param buildFinishDate 完工日期
     */
    public void setBuildFinishDate(Date buildFinishDate) {
        this.buildFinishDate = buildFinishDate;
    }

    /**
     * 交房日期
     * @return hand_in_date 交房日期
     */
    public Date getHandInDate() {
        return handInDate;
    }

    /**
     * 交房日期
     * @param handInDate 交房日期
     */
    public void setHandInDate(Date handInDate) {
        this.handInDate = handInDate;
    }

    /**
     * 创建时间
     * @return create_time 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", projectName=").append(projectName);
        sb.append(", maxAmount=").append(maxAmount);
        sb.append(", minAmount=").append(minAmount);
        sb.append(", averageAmount=").append(averageAmount);
        sb.append(", address=").append(address);
        sb.append(", floorType=").append(floorType);
        sb.append(", type=").append(type);
        sb.append(", addrProvince=").append(addrProvince);
        sb.append(", addrCity=").append(addrCity);
        sb.append(", addrDistrict=").append(addrDistrict);
        sb.append(", sysState=").append(sysState);
        sb.append(", buildState=").append(buildState);
        sb.append(", busState=").append(busState);
        sb.append(", longitude=").append(longitude);
        sb.append(", latitude=").append(latitude);
        sb.append(", salesCall=").append(salesCall);
        sb.append(", buildStartDate=").append(buildStartDate);
        sb.append(", openingDate=").append(openingDate);
        sb.append(", buildFinishDate=").append(buildFinishDate);
        sb.append(", handInDate=").append(handInDate);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append("]");
        return sb.toString();
    }
}